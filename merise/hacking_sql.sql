#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: network
#------------------------------------------------------------

CREATE TABLE network(
        id             Int  Auto_increment  NOT NULL ,
        label          Varchar (50) NOT NULL ,
        flavored_label Varchar (50) NOT NULL
	,CONSTRAINT network_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: level
#------------------------------------------------------------

CREATE TABLE level(
        id         Int  Auto_increment  NOT NULL ,
        depth      Int NOT NULL ,
        isBottom   Bool NOT NULL ,
        id_network Int NOT NULL
	,CONSTRAINT level_PK PRIMARY KEY (id)

	,CONSTRAINT level_network_FK FOREIGN KEY (id_network) REFERENCES network(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: controlNode
#------------------------------------------------------------

CREATE TABLE controlNode(
        id          Int  Auto_increment  NOT NULL ,
        targetLabel Varchar (50) NOT NULL ,
        challenge   Int NOT NULL ,
        id_level    Int NOT NULL
	,CONSTRAINT controlNode_PK PRIMARY KEY (id)

	,CONSTRAINT controlNode_level_FK FOREIGN KEY (id_level) REFERENCES level(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: dataFile
#------------------------------------------------------------

CREATE TABLE dataFile(
        id        Int  Auto_increment  NOT NULL ,
        label     Varchar (50) NOT NULL ,
        content   Text NOT NULL ,
        challenge Int NOT NULL ,
        id_level  Int NOT NULL
	,CONSTRAINT dataFile_PK PRIMARY KEY (id)

	,CONSTRAINT dataFile_level_FK FOREIGN KEY (id_level) REFERENCES level(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: accessLock
#------------------------------------------------------------

CREATE TABLE accessLock(
        id        Int  Auto_increment  NOT NULL ,
        challenge Int NOT NULL ,
        password  Varchar (255) NOT NULL ,
        id_level  Int NOT NULL
	,CONSTRAINT accessLock_PK PRIMARY KEY (id)

	,CONSTRAINT accessLock_level_FK FOREIGN KEY (id_level) REFERENCES level(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: netrunner
#------------------------------------------------------------

CREATE TABLE netrunner(
        id        Int  Auto_increment  NOT NULL ,
        name      Varchar (50) ,
        skillRank Int NOT NULL
	,CONSTRAINT netrunner_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: blackIceData
#------------------------------------------------------------

CREATE TABLE blackIceData(
        id         Int  Auto_increment  NOT NULL ,
        className  Varchar (20) NOT NULL ,
        perception Int NOT NULL ,
        speed      Int NOT NULL ,
        attack     Int NOT NULL ,
        defense    Int NOT NULL ,
        rez        Int NOT NULL ,
        targetType Varchar (20) NOT NULL ,
        effectText Text NOT NULL
	,CONSTRAINT blackIceData_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: virus
#------------------------------------------------------------

CREATE TABLE virus(
        id           Int  Auto_increment  NOT NULL ,
        challenge    Int NOT NULL ,
        label        Varchar (20) NOT NULL ,
        effects      Text ,
        id_network   Int NOT NULL ,
        id_netrunner Int NOT NULL
	,CONSTRAINT virus_PK PRIMARY KEY (id)

	,CONSTRAINT virus_network_FK FOREIGN KEY (id_network) REFERENCES network(id)
	,CONSTRAINT virus_netrunner0_FK FOREIGN KEY (id_netrunner) REFERENCES netrunner(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: blackIceInstance
#------------------------------------------------------------

CREATE TABLE blackIceInstance(
        id              Int  Auto_increment  NOT NULL ,
        currentRez      Int NOT NULL ,
        id_blackIceData Int NOT NULL ,
        id_level        Int NOT NULL
	,CONSTRAINT blackIceInstance_PK PRIMARY KEY (id)

	,CONSTRAINT blackIceInstance_blackIceData_FK FOREIGN KEY (id_blackIceData) REFERENCES blackIceData(id)
	,CONSTRAINT blackIceInstance_level0_FK FOREIGN KEY (id_level) REFERENCES level(id)
)ENGINE=InnoDB;

