import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';

import { NetworksService } from './services/networks.service';
import { FloorsService } from './services/floors.service';
import { AccessLocksService } from './services/accessLocks.service';
import { BlackIceDatasService } from './services/blackIceDatas.service';
import { BlackIceInstancesService } from './services/blackIceInstances.service';
import { ControlNodesService } from './services/controlNodes.service';
import { DataFilesService } from './services/dataFiles.service';

import { PageLandingComponent } from './page-landing/page-landing.component';
import { PageAdminComponent } from './page-admin/page-admin.component';
import { CreateNetworkComponent } from './create-network/create-network.component';
import { CreateFloorComponent } from './create-floor/create-floor.component';
import { KitchenSinkComponent } from './kitchen-sink/kitchen-sink.component';
import { FloorNodeComponent } from './floor-node/floor-node.component';
import { EditNetworkComponent } from './edit-network/edit-network.component';


@NgModule({
  declarations: [
    AppComponent,
    PageLandingComponent,
    PageAdminComponent,
    CreateNetworkComponent,
    CreateFloorComponent,
    KitchenSinkComponent,
    FloorNodeComponent,
    EditNetworkComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    NetworksService,
    FloorsService,
    AccessLocksService,
    BlackIceDatasService,
    BlackIceInstancesService,
    ControlNodesService,
    DataFilesService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
