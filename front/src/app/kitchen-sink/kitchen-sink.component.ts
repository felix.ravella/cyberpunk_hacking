import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';

import { FloorNodeComponent } from '../floor-node/floor-node.component';

@Component({
  selector: 'app-kitchen-sink',
  templateUrl: './kitchen-sink.component.html',
  styleUrls: ['./kitchen-sink.component.scss']
})
export class KitchenSinkComponent implements OnInit {
  nodes: Array<any> = [];

  @ViewChildren('floorNode') floorNodes?: QueryList<FloorNodeComponent>;

  constructor() { }

  ngOnInit(): void {
  }

  newFloorNode() {
    this.nodes.push({nodeType: ""});
  }
}
