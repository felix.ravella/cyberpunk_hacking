import { Component, OnInit, Input, Output, ViewChildren, QueryList, EventEmitter } from '@angular/core';


import { FloorNodeComponent } from '../floor-node/floor-node.component';

@Component({
  selector: 'app-create-floor',
  templateUrl: './create-floor.component.html',
  styleUrls: ['./create-floor.component.scss']
})
export class CreateFloorComponent implements OnInit {
  @Input() id = -1;
  @Input() depth: number = 1;

  @Output() removeFloorEmitter: EventEmitter<number> = new EventEmitter<number>();

  nodes: Array<any> = [];
  @ViewChildren('floorNode') floorNodes?: QueryList<FloorNodeComponent>;

  constructor() { }

  ngOnInit(): void {
  }

  newFloorNode() {
    this.nodes.push({nodeType: ""});
  }

  removeFloor() {
    this.removeFloorEmitter.emit(this.depth - 1);
  }

  onRemoveNodeEvent(index: number) {
    this.nodes.splice(index, 1);

  }

}
