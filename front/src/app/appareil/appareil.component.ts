import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

  @Input() appareilName: string ="";
  @Input() status: string = "éteint";
  constructor() { }

  secondes: number = 0;

  ngOnInit(): void {
  }

  getStatus() {
    return this.status;
  }

  getColor() {
    return (this.status === "éteint") ? "red" : "green";
  }
}
