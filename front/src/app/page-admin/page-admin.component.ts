import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NetworksService } from '../services/networks.service';
import { Network } from '../classes/network.class';

@Component({
  selector: 'app-page-admin',
  templateUrl: './page-admin.component.html',
  styleUrls: ['./page-admin.component.scss']
})
export class PageAdminComponent implements OnInit {

  networks = new Array<Network>();

  constructor(
    private networksService: NetworksService,
    private router: Router,
  ){ }

  ngOnInit(): void {
    this.networksService.getNetworks()
      .subscribe(response => {
        this.networks = response.map(item => {
          {
            return new Network(
              item.id,
              item.name,
              item.label,
              item.Floors
            )
          }
        })
    })
  }

  goToCreate() {
    this.router.navigateByUrl('admin/createNetwork');
  }


}
