import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { ControlNode } from '../classes/controlNode.class'

@Injectable()
export class ControlNodesService {

  constructor(
    private httpClient: HttpClient
  ) {}

  public createControlNode(payload: Record<string,any>): Observable<ControlNode>{
    return this.httpClient
      .post<ControlNode>(`${environment.apiURL}/controlNodes`, payload);
  }

  public getControlNodes(): Observable<ControlNode[]>{
    return this.httpClient
      .get<ControlNode[]>(`${environment.apiURL}/controlNodes`);
  }

  public getControlNodesByNetworkId(id: number): Observable<ControlNode[]>{
    return this.httpClient
      .get<ControlNode[]>(`${environment.apiURL}/controlNodes/networks/${id}`);
  }

  public getControlNode(id: number): Observable<ControlNode>{
    return this.httpClient
      .get<ControlNode>(`${environment.apiURL}/controlNodes/${id}`)
  }

  public updateControlNode(id: number, payload: Record<string,any>): Observable<ControlNode> {
    return this.httpClient
      .put<ControlNode>(`${environment.apiURL}/controlNodes/${id}`, payload)
  }
}
