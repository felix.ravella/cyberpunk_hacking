import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { Floor } from '../classes/floor.class'

@Injectable()
export class FloorsService {

  constructor(
    private httpClient: HttpClient
  ) {}

  public createFloor(payload: Record<string,any>): Observable<Floor>{
    return this.httpClient
      .post<Floor>(`${environment.apiURL}/floors`, payload);
  }

  public getFloors(): Observable<Floor[]>{
    return this.httpClient
      .get<Floor[]>(`${environment.apiURL}/floors`);
  }

  public getFloorsByNetworkId(id: number): Observable<Floor[]>{
    return this.httpClient
      .get<Floor[]>(`${environment.apiURL}/floors/networks/${id}`);
  }

  public getFloor(id: number): Observable<Floor>{
    return this.httpClient
      .get<Floor>(`${environment.apiURL}/floors/${id}`)
  }

  public updateFloor(id: number, payload: Record<string,any>): Observable<Floor> {
    return this.httpClient
      .put<Floor>(`${environment.apiURL}/floors/${id}`, payload)
  }

  public deleteFloor(id: number): Observable<Floor> {
    return this.httpClient
      .delete<Floor>(`${environment.apiURL}/floors/${id}`)
  }

}
