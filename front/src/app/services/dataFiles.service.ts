import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { DataFile } from '../classes/dataFile.class'

@Injectable()
export class DataFilesService {

  constructor(
    private httpClient: HttpClient
  ) {}

  public createDataFile(payload: Record<string,any>): Observable<DataFile>{
    return this.httpClient
      .post<DataFile>(`${environment.apiURL}/dataFiles`, payload);
  }

  public getDataFiles(): Observable<DataFile[]>{
    return this.httpClient
      .get<DataFile[]>(`${environment.apiURL}/dataFiles`);
  }

  public getDataFilesByNetworkId(id: number): Observable<DataFile[]>{
    return this.httpClient
      .get<DataFile[]>(`${environment.apiURL}/dataFiles/networks/${id}`);
  }

  public getDataFile(id: number): Observable<DataFile>{
    return this.httpClient
      .get<DataFile>(`${environment.apiURL}/dataFiles/${id}`)
  }

  public updateDataFile(id: number, payload: Record<string,any>): Observable<DataFile> {
    return this.httpClient
      .put<DataFile>(`${environment.apiURL}/dataFiles/${id}`, payload)
  }
}
