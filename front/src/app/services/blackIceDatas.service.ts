import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { BlackIceData } from '../classes/blackIceData.class'

@Injectable()
export class BlackIceDatasService {

  private networks = []

  constructor(
    private httpClient: HttpClient
  ) {}

  public createBlackIceData(payload: Record<string,any>): Observable<BlackIceData>{
    return this.httpClient
      .post<BlackIceData>(`${environment.apiURL}/blackIceDatas`, payload);
  }

  public getBlackIceDatas(): Observable<BlackIceData[]>{
    return this.httpClient
      .get<BlackIceData[]>(`${environment.apiURL}/blackIceDatas`);
  }

  public getBlackIceDatasByNetworkId(id: number): Observable<BlackIceData[]>{
    return this.httpClient
      .get<BlackIceData[]>(`${environment.apiURL}/blackIceDatas/networks/${id}`);
  }

  public getBlackIceData(id: number): Observable<BlackIceData>{
    return this.httpClient
      .get<BlackIceData>(`${environment.apiURL}/blackIceDatas/${id}`)
  }

  public updateBlackIceData(id: number, payload: Record<string,any>): Observable<BlackIceData> {
    return this.httpClient
      .put<BlackIceData>(`${environment.apiURL}/blackIceDatas/${id}`, payload)

  }

}
