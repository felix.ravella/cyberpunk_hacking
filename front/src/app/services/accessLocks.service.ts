import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { AccessLock } from '../classes/accessLock.class'

@Injectable()
export class AccessLocksService {

  constructor(
    private httpClient: HttpClient
  ) {}

  public createAccessLock(payload: Record<string,any>): Observable<AccessLock>{
    return this.httpClient
      .post<AccessLock>(`${environment.apiURL}/accessLocks`, payload);
  }

  public getAccessLocks(): Observable<AccessLock[]>{
    return this.httpClient
      .get<AccessLock[]>(`${environment.apiURL}/accessLocks`);
  }

  public getAccessLocksByNetworkId(id: number): Observable<AccessLock[]>{
    return this.httpClient
      .get<AccessLock[]>(`${environment.apiURL}/accessLocks/networks/${id}`);
  }

  public getAccessLock(id: number): Observable<AccessLock>{
    return this.httpClient
      .get<AccessLock>(`${environment.apiURL}/accessLocks/${id}`)
  }

  public updateAccessLock(id: number, payload: Record<string,any>): Observable<AccessLock> {
    return this.httpClient
      .put<AccessLock>(`${environment.apiURL}/accessLocks/${id}`, payload)

  }

}
