import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { Network } from '../classes/network.class'

@Injectable()
export class NetworksService {

  private networks = []

  constructor(
    private httpClient: HttpClient
  ) {}

  public createNetwork(payload: Record<string,any>): Observable<Network>{
    return this.httpClient
      .post<Network>(`${environment.apiURL}/networks`, payload);
  }

  public getNetworks(): Observable<Network[]>{
    return this.httpClient
      .get<Network[]>(`${environment.apiURL}/networks`);
  }

  public getNetwork(id: number): Observable<Network>{
    return this.httpClient
      .get<Network>(`${environment.apiURL}/networks/${id}`)
  }

  public updateNetwork(id: number, payload: Record<string,any>): Observable<Network> {
    return this.httpClient
      .put<Network>(`${environment.apiURL}/networks/${id}`, payload)
  }

  public deleteNetwork(id: number): Observable<Network> {
    return this.httpClient
      .delete<Network>(`${environment.apiURL}/networks/${id}`)
  }

}
