import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { BlackIceInstance } from '../classes/blackIceInstance.class'

@Injectable()
export class BlackIceInstancesService {

  constructor(
    private httpClient: HttpClient
  ) {}

  public createBlackIceInstance(payload: Record<string,any>): Observable<BlackIceInstance>{
    return this.httpClient
      .post<BlackIceInstance>(`${environment.apiURL}/blackIceInstances`, payload);
  }

  public getBlackIceInstances(): Observable<BlackIceInstance[]>{
    return this.httpClient
      .get<BlackIceInstance[]>(`${environment.apiURL}/blackIceInstances`);
  }

  public getBlackIceInstancesByNetworkId(id: number): Observable<BlackIceInstance[]>{
    return this.httpClient
      .get<BlackIceInstance[]>(`${environment.apiURL}/blackIceInstances/networks/${id}`);
  }

  public getBlackIceInstance(id: number): Observable<BlackIceInstance>{
    return this.httpClient
      .get<BlackIceInstance>(`${environment.apiURL}/blackIceInstances/${id}`)
  }

  public updateBlackIceInstance(id: number, payload: Record<string,any>): Observable<BlackIceInstance> {
    return this.httpClient
      .put<BlackIceInstance>(`${environment.apiURL}/blackIceInstances/${id}`, payload)
  }
}
