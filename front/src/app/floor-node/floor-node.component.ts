import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { BlackIceData } from '../classes/blackIceData.class';
import { BlackIceDatasService } from '../services/blackIceDatas.service';


@Component({
  selector: 'app-floor-node',
  templateUrl: './floor-node.component.html',
  styleUrls: ['./floor-node.component.scss']
})
export class FloorNodeComponent implements OnInit {
  nodeTypeDictionary: Array<Record<string,any>> = [
    {type:"accessLock", optionLabel:"Verrou", challenge: 0, password: ""},
    {type:"blackIceInstance", optionLabel: "Black Ice", id_blackIceData: 0},
    {type:"controlNode", optionLabel: "Noeud de Controle", challenge: 0, targetLabel: ""},
    {type:"dataFile", optionLabel:"Fichier de données", challenge: 0, label: "", content: ""},
  ]

  @Input() index: number = 0;

  @Input() nodeData: Record<string,any> = {type:""};

  blackIceDatas: Array<BlackIceData> = [];
  deletedNodeDatas: Record<string,any>[] = [];

  @Output() removeNodeEmitter: EventEmitter<number> = new EventEmitter<number>();

  constructor(
    private blackIceDatasService: BlackIceDatasService,
  ) { }

  ngOnInit(): void {
    this.blackIceDatasService.getBlackIceDatas()
      .subscribe(response => {
        this.blackIceDatas = response.map(item => {
          {
            return new BlackIceData(
              item.id,
              item.className,
              item.perception,
              item.speed,
              item.attack,
              item.defense,
              item.maxRez,
              item.targetType,
              item.effectText,
            )
          }
        })
    })
  }

  onTypeChange(event: any) {
    if (event.target.value === "") {
      this.nodeData = {type: ""};
    } else {
      this.nodeData = this.nodeTypeDictionary[event.target.value];
    }
  }

  onBlackIceDataChange(event: any) {
    this.nodeData['id_blackIceData'] = event.target.value;
  }

  removeNode() {
    this.removeNodeEmitter.emit(this.index);
  }
}
