import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { NetworksService } from '../services/networks.service';
import { FloorsService } from '../services/floors.service';
import { AccessLocksService } from '../services/accessLocks.service';
import { BlackIceInstancesService } from '../services/blackIceInstances.service';
import { ControlNodesService } from '../services/controlNodes.service';
import { DataFilesService } from '../services/dataFiles.service';

import { Floor } from '../classes/floor.class';
import { CreateFloorComponent } from '../create-floor/create-floor.component';

@Component({
  selector: 'app-create-network',
  templateUrl: './create-network.component.html',
  styleUrls: ['./create-network.component.scss']
})
export class CreateNetworkComponent implements OnInit {
  name: any;
  label: any;

  floors: Array<Floor> = [];
  @ViewChildren('floor') floorComponents?: QueryList<CreateFloorComponent>;

  constructor(
    private networksService: NetworksService,
    private floorsService: FloorsService,
    private accessLocksService: AccessLocksService,
    private blackIceInstancesService: BlackIceInstancesService,
    private controlNodesService: ControlNodesService,
    private dataFilesService: DataFilesService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  addFloor() {
    this.floors.push(new Floor(-1, -1, false, -1, [], [], [], []));
  }

  onRemoveFloorEvent(index: number) {
    this.floors.splice(index, 1);
  }

  onCreateButton(): void {
    try {
      let networkPayload = {name: this.name, label: this.label};
      // creation du network
      this.networksService.createNetwork(networkPayload)
        .subscribe(newNetwork => {
          if (!!this.floorComponents){
            let maxDepth = this.floorComponents.length
            this.floorComponents.map(floor => {
              let floorPayload = {
                depth: floor.depth,
                isBottom: floor.depth == maxDepth,
                id_network: newNetwork.id
              }
              // creation des floors
              this.floorsService.createFloor(floorPayload)
                .subscribe(newFloor => {
                  if (!!floor.floorNodes) {
                    floor.floorNodes.map(node => {
                      let {type, optionlabel, ...rest} = node.nodeData;
                      let nodePayload = {id_floor: newFloor.id, ...rest};
                      switch (node.nodeData['type']){
                        case 'accessLock' :
                          this.accessLocksService.createAccessLock(nodePayload)
                            .subscribe();
                          break;
                        case 'blackIceInstance' :
                          this.blackIceInstancesService.createBlackIceInstance(nodePayload)
                            .subscribe();
                          break;
                        case 'controlNode' :
                          this.controlNodesService.createControlNode(nodePayload)
                            .subscribe();
                          break;
                        case 'dataFile' :
                          this.dataFilesService.createDataFile(nodePayload)
                            .subscribe();
                          break;
                      }
                    })
                  }
                });
            })
          }
        })
      this.toastr.success("Le reseau a été créée", "Succès!");
      this.router.navigateByUrl('admin')
    } catch (error) {
      this.toastr.error("Une erreur est survenue", "Oups!");
    }
  }

  goBack() {
    this.router.navigateByUrl('admin')
  }
}
