import { Floor } from "../classes/floor.class"

export class Network {
  constructor(
    public id: number,
    public name: string,
    public label: string,
    public Floors: Floor[]
  ) {}
}
