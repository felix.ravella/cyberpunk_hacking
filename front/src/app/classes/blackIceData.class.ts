export class BlackIceData {
  constructor(
    public id: number,
    public className: string,
    public perception: number,
    public speed: number,
    public attack: number,
    public defense: number,
    public maxRez: number,
    public targetType: string,
    public effectText: string,
  ) {}
}
