import { AccessLock } from "../classes/accessLock.class"
import { BlackIceInstance } from "../classes/blackIceInstance.class"
import { ControlNode } from "../classes/controlNode.class"
import { DataFile } from "../classes/dataFile.class"

export class Floor {
  constructor(
    public id: number,
    public depth: number,
    public isBottom: boolean,
    public id_network: number,
    public AccessLocks: AccessLock[],
    public BlackIceInstances: BlackIceInstance[],
    public ControlNodes: ControlNode[],
    public DataFiles: DataFile[],
  ) {}
}
