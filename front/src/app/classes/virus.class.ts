export class Virus {
  constructor(
    public id: number,
    public challenge: number,
    public label: string,
    public effects: string,
    public idNetwork: number,
    public idNetrunner: number
  ) {}
}
