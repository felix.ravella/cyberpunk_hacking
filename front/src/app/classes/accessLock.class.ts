export class AccessLock {
  constructor(
    public id: number,
    public challenge: number,
    public password: string,
    public id_floor: number
  ) {}
}
