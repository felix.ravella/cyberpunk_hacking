export class DataFile {
  constructor(
    public id: number,
    public label: string,
    public content: string,
    public challenge: number,
    public id_floor: number
  ) {}
}
