import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { timer } from 'rxjs';

import { NetworksService } from '../services/networks.service';
import { FloorsService } from '../services/floors.service';
import { AccessLocksService } from '../services/accessLocks.service';
import { BlackIceInstancesService } from '../services/blackIceInstances.service';
import { ControlNodesService } from '../services/controlNodes.service';
import { DataFilesService } from '../services/dataFiles.service';

import { Floor } from '../classes/floor.class';
import { AccessLock } from '../classes/accessLock.class';
import { BlackIceInstance } from '../classes/blackIceInstance.class';
import { ControlNode } from '../classes/controlNode.class';
import { DataFile } from '../classes/dataFile.class';

import { CreateFloorComponent } from '../create-floor/create-floor.component';

@Component({
  selector: 'app-edit-network',
  templateUrl: './edit-network.component.html',
  styleUrls: ['./edit-network.component.scss']
})
export class EditNetworkComponent implements OnInit {
  networkId: any;
  name: String = "";
  label: String = "";
  deletedFloors: Array<number> = [];

  floors: Array<Floor> = [];
  @ViewChildren('floor') floorComponents?: QueryList<CreateFloorComponent>;

  constructor(
    private route: ActivatedRoute,
    private networksService: NetworksService,
    private floorsService: FloorsService,
    private accessLocksService: AccessLocksService,
    private blackIceInstancesService: BlackIceInstancesService,
    private controlNodesService: ControlNodesService,
    private dataFilesService: DataFilesService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    let tempNodesArray = new Array();
    this.route.queryParams.subscribe(params => {
      this.networkId = this.route.snapshot.paramMap.get('id');
    });
    this.networksService.getNetwork(this.networkId)
      .subscribe(response => {
          this.name = response.name;
          this.label = response.label;
          this.floors = response.Floors.map(floor => {
            let nodesInCurrentFloor: Record<string,any>[] = []; // on créé un array qui correspond à l'etage
            let newFloor = new Floor(
              floor.id,
              floor.depth,
              floor.isBottom,
              floor.id_network,
              floor.AccessLocks.map(acc => {
                // creation d'un hash avec les données, et push dans le array du floor
                let newAccessLock = new AccessLock(
                  acc.id,
                  acc.challenge,
                  acc.password,
                  acc.id_floor
                );
                nodesInCurrentFloor.push({type: "accessLock", optionLabel: "Verrou", challenge: acc.challenge, password: acc.password});
                return newAccessLock
              }),
              floor.BlackIceInstances.map(blac => {
                let newBlackIceInstance = new BlackIceInstance(
                  blac.id,
                  blac.id_blackIceData,
                  blac.id_floor
                );
                nodesInCurrentFloor.push({type: "blackIceInstance", optionLabel: "Black Ice", id_blackIceData: blac.id_blackIceData });
                return newBlackIceInstance;
              }),
              floor.ControlNodes.map(cont => {
                let newControlNode = new ControlNode(
                  cont.id,
                  cont.targetLabel,
                  cont.challenge,
                  cont.id_floor
                );
                nodesInCurrentFloor.push({type:"controlNode", optionLabel: "Noeud de Controle", challenge: cont.challenge, targetLabel: cont.targetLabel});
                return newControlNode;
              }),
              floor.DataFiles.map(file => {
                let newDataFile = new DataFile(
                  file.id,
                  file.label,
                  file.content,
                  file.challenge,
                  file.id_floor
                );
                nodesInCurrentFloor.push({type:"dataFile", optionLabel:"Fichier de données", challenge: file.challenge, label: file.challenge, content: file.content});
                return newDataFile;
              })
            );
            tempNodesArray.push(nodesInCurrentFloor); // push du floor dans le array des data de floor
            return newFloor;
          });

          // tmer poura ttendre que les floors soient instanciés
          const clock = timer(50);
          clock.subscribe(_ => {
            if (!!this.floorComponents) {
              let i = 0;
              this.floorComponents.map(item => {
                item.nodes = tempNodesArray[i];
                i++;
              })
            }
          });
        });

  }

  addFloor() {
    this.floors.push(new Floor(-1, -1, false, -1, [], [], [], []));
  }

  onRemoveFloorEvent(index: number) {
    let deletedFloor = this.floors.splice(index, 1);
    if (deletedFloor[0].id != -1) {
      this.deletedFloors.push(deletedFloor[0].id);
    }
  }

  onConfirmButton() {
    try {
      this.deletedFloors.map((item: number) => {
        this.floorsService.deleteFloor(item)
          .subscribe(data => {} );
      });
      let networkPayload = {name: this.name, label: this.label};
      // update du network
      this.networksService.updateNetwork(this.networkId, networkPayload)
        .subscribe(response => {
          // suppression des floors retirés
          if (!!this.floorComponents) {
            let maxDepth = this.floorComponents.length
            this.floorComponents.map(floor => {
              // suppression du floor s'il n'est pas nouveau
              if (floor.id != -1) {
                this.floorsService.deleteFloor(floor.id).subscribe();
              }
              //(re)création du floor
              let floorPayload = {
                depth: floor.depth,
                isBottom: floor.depth == maxDepth,
                id_network: this.networkId
              }
              this.floorsService.createFloor(floorPayload)
                .subscribe(newFloor => {
                  if (!!floor.floorNodes) {
                    floor.floorNodes.map(node => {
                      let {type, optionlabel, ...rest} = node.nodeData;
                      let nodePayload = {id_floor: newFloor.id, ...rest};
                      switch (node.nodeData['type']){
                        case 'accessLock' :
                          this.accessLocksService.createAccessLock(nodePayload)
                            .subscribe();
                          break;
                        case 'blackIceInstance' :
                          this.blackIceInstancesService.createBlackIceInstance(nodePayload)
                            .subscribe();
                          break;
                        case 'controlNode' :
                          this.controlNodesService.createControlNode(nodePayload)
                            .subscribe();
                          break;
                        case 'dataFile' :
                          this.dataFilesService.createDataFile(nodePayload)
                            .subscribe();
                          break;
                      }
                    });
                  }
                });
            })
          }
        });
      this.toastr.success("Le réseau a été modifié", "Succès!");
      this.router.navigateByUrl('admin')
    } catch (error) {
      this.toastr.error("Une erreur est survenue", "Oups!");
    }
  }

  goBack() {
    this.router.navigateByUrl('admin')
  }

  deleteNetwork() {
    try {
      this.networksService.deleteNetwork(this.networkId)
        .subscribe();
      this.toastr.success("Le reseau a été supprimé", "Succès!");
      this.router.navigateByUrl('admin')
    } catch (error) {
      this.toastr.error("Une erreur est survenue", "Oups!");
    }
  }

}
