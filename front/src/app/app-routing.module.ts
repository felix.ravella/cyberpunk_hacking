import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageLandingComponent } from './page-landing/page-landing.component';
import { PageAdminComponent } from './page-admin/page-admin.component';
import { CreateNetworkComponent } from './create-network/create-network.component';
import { EditNetworkComponent } from './edit-network/edit-network.component';

import { KitchenSinkComponent } from './kitchen-sink/kitchen-sink.component';

const routes: Routes = [
  { path: 'admin', component: PageAdminComponent },
  { path: 'admin/createNetwork', component: CreateNetworkComponent },
  { path: 'admin/editNetwork/:id', component: EditNetworkComponent },
  { path: 'test', component: KitchenSinkComponent },
  { path: '', component: PageLandingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
