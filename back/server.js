const cors = require('cors');
const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.use(cors());


// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

require("./routes/netrunners.route.js")(app);
require("./routes/virus.route.js")(app);
require("./routes/accessLocks.route.js")(app);
require("./routes/blackIceDatas.route.js")(app);
require("./routes/blackIceInstances.route.js")(app);
require("./routes/controlNodes.route.js")(app);
require("./routes/dataFiles.route.js")(app);
require("./routes/floors.route.js")(app);
require("./routes/networks.route.js")(app);

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Voci la page d'accueil." });
});

// set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});
