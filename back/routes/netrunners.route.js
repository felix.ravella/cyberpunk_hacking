module.exports = app => {
  const netrunnerController = require("../controllers/netrunners.controller.js");

  app.get("/netrunners/:id", netrunnerController.findOne);

  app.get("/netrunners", netrunnerController.findAll);

  app.post("/netrunners", netrunnerController.create);

  app.delete("/netrunners/:id", netrunnerController.deleteOne);

  app.put("/netrunners/:id", netrunnerController.update);
  
};
