module.exports = app => {
  const accessLockController = require("../controllers/accessLocks.controller.js");

  app.get("/accessLocks/:id", accessLockController.findOne);

  app.get("/accessLocks", accessLockController.findAll);

  app.post("/accessLocks", accessLockController.create);

  app.delete("/accessLocks/:id", accessLockController.deleteOne);

  app.put("/accessLocks/:id", accessLockController.update);
  
};
