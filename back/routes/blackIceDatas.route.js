module.exports = app => {
  const blackIceDataController = require("../controllers/blackIceDatas.controller.js");

  app.post("/blackIceDatas", blackIceDataController.create);

  app.get("/blackIceDatas", blackIceDataController.findAll);

  app.get("/blackIceDatas/:id", blackIceDataController.findOne);

  app.put("/blackIceDatas/:id", blackIceDataController.update);
  
  app.delete("/blackIceDatas/:id", blackIceDataController.deleteOne);
  
};
