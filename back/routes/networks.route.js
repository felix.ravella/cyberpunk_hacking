module.exports = app => {
  const networkController = require("../controllers/networks.controller.js");

  app.get("/networks/:id", networkController.findOne);

  app.get("/networks", networkController.findAll);

  app.post("/networks", networkController.create);

  app.delete("/networks/:id", networkController.deleteOne);

  app.put("/networks/:id", networkController.update);
  
};
