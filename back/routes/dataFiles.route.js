module.exports = app => {
  const dataFileController = require("../controllers/dataFiles.controller.js");

  app.get("/dataFiles/:id", dataFileController.findOne);

  app.get("/dataFiles", dataFileController.findAll);

  app.post("/dataFiles", dataFileController.create);

  app.delete("/dataFiles/:id", dataFileController.deleteOne);

  app.put("/dataFiles/:id", dataFileController.update);
  
};
