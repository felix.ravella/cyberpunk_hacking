module.exports = app => {
  const floorController = require("../controllers/floors.controller.js");

  app.get("/floors/:id", floorController.findOne);
  app.get("/floors", floorController.findAll);
  app.get("/floors/networks/:id", floorController.findAllByNetworkId);

  app.post("/floors", floorController.create);

  app.delete("/floors/:id", floorController.deleteOne);

  app.put("/floors/:id", floorController.update);
  
};
