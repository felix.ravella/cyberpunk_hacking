module.exports = app => {
  const blackIceInstanceController = require("../controllers/blackIceInstances.controller.js");

  app.post("/blackIceInstances", blackIceInstanceController.create);

  app.get("/blackIceInstances", blackIceInstanceController.findAll);

  app.get("/blackIceInstances/:id", blackIceInstanceController.findOne);

  app.put("/blackIceInstances/:id", blackIceInstanceController.update);
  
  app.delete("/blackIceInstances/:id", blackIceInstanceController.deleteOne);
  
};
