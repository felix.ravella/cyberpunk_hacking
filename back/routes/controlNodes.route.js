module.exports = app => {
  const controlNodeController = require("../controllers/controlNodes.controller.js");

  app.get("/controlNodes/:id", controlNodeController.findOne);

  app.get("/controlNodes", controlNodeController.findAll);

  app.post("/controlNodes", controlNodeController.create);

  app.delete("/controlNodes/:id", controlNodeController.deleteOne);

  app.put("/controlNodes/:id", controlNodeController.update);
  
};
