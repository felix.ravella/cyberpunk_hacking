module.exports = app => {
  const virusController = require("../controllers/virus.controller.js");

  app.get("/virus/:id", virusController.findOne);

  app.get("/virus", virusController.findAll);

  app.post("/virus", virusController.create);

  app.delete("/virus/:id", virusController.deleteOne);

  app.put("/virus/:id", virusController.update);
  
};
