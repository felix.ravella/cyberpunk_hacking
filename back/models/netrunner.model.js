const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const Virus = require("../models/virus.model.js");

const Netrunner = sequelize.define('Netrunner', {
  name: { type: DataTypes.STRING },
  skillRank: { type: DataTypes.INTEGER }
},{
  tableName: 'netrunner',
  timestamps: false,
});

Netrunner.hasMany(Virus, {
  foreignKey: 'id_netrunner'
});



module.exports = Netrunner;
