const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const ControlNode = sequelize.define('ControlNode', {
  challenge : { type: DataTypes.INTEGER },
  targetLabel : { type: DataTypes.STRING },
}, {
  tableName: 'controlNode',
  timestamps: false,
});

module.exports = ControlNode;
