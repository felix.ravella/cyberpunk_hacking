const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const Virus = require("../models/virus.model.js");
const Floor = require("../models/floor.model.js");

const Network = sequelize.define('Network', {
  label: { type: DataTypes.STRING },
  name: { type: DataTypes.STRING }
},{
  tableName: 'network',
  timestamps: false,
});

Network.hasMany(Floor, {
  foreignKey: 'id_network'
});
Network.hasMany(Virus, {
  foreignKey: 'id_network'
});

module.exports = Network;
