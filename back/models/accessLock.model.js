const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const AccessLock = sequelize.define('AccessLock', {
  challenge: { type: DataTypes.INTEGER },
  password: { type: DataTypes.STRING }
}, {
  tableName: 'accessLock',
  timestamps: false,
});

module.exports = AccessLock;
