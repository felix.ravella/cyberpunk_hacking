const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const DataFile = sequelize.define('DataFile', {
  label: { type: DataTypes.STRING },
  content: { type: DataTypes.TEXT },
  cyphered_content: { type: DataTypes.TEXT },
  challenge: { type: DataTypes.INTEGER },
},{
  tableName: 'dataFile',
  timestamps: false,
});

module.exports = DataFile;
