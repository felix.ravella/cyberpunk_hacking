const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const BlackIceInstance = sequelize.define('BlackIceInstance', {
},{
  tableName: 'blackIceInstance',
  timestamps: false,
});

module.exports = BlackIceInstance;
