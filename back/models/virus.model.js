const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const Virus = sequelize.define('Virus', {
  challenge: { type: DataTypes.INTEGER },
  label: { type: DataTypes.STRING },
  effects: { type: DataTypes.TEXT }
}, {
  tableName: 'virus',
  timestamps: false,
});

module.exports = Virus;
