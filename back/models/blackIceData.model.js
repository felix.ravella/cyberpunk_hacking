const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const BlackIceInstance = require("../models/blackIceInstance.model.js"); 

const BlackIceData = sequelize.define('BlackIceData', {
  className: { type: DataTypes.STRING },
  perception: { type: DataTypes.INTEGER },
  speed: { type: DataTypes.INTEGER },
  attack: { type: DataTypes.INTEGER },
  defense: { type: DataTypes.INTEGER },
  maxRez: { type: DataTypes.INTEGER },
  targetType: { type: DataTypes.STRING },
  effectText: { type: DataTypes.TEXT, defaultValue: "" },
}, {
  tableName: 'blackIceData',
  timestamps: false,
});

BlackIceData.hasMany(BlackIceInstance, {
  foreignKey: 'id_blackIceData'
});

module.exports = BlackIceData;
