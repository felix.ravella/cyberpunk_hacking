const dbConfig = require("../db.config.js");
const { Sequelize } = require('sequelize');

const connection = new Sequelize(
  dbConfig.DB,
  dbConfig.USER,
  dbConfig.PASSWORD,
  {
    dialect: 'mysql',
    host: dbConfig.HOST,
  },
);

module.exports = connection;
