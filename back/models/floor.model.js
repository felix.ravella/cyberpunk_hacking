const sequelize = require("./db.js");
const { DataTypes } = require("sequelize");

const AccessLock = require("../models/accessLock.model.js");
const BlackIceInstance = require("../models/blackIceInstance.model.js");
const ControlNode = require("../models/controlNode.model.js");
const DataFile = require("../models/dataFile.model.js");

const Floor = sequelize.define('Floor', {
  depth: { type: DataTypes.INTEGER },
  isBottom: { type: DataTypes.BOOLEAN },
},{
  tableName: 'floor',
  timestamps: false,
});

Floor.hasMany(AccessLock, {
  foreignKey: 'id_floor'
});

Floor.hasMany(BlackIceInstance, {
  foreignKey: 'id_floor'
});

Floor.hasMany(ControlNode, {
  foreignKey: 'id_floor'
});

Floor.hasMany(DataFile, {
  foreignKey: 'id_floor'
});

module.exports = Floor;
