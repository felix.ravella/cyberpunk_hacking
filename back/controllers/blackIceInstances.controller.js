const BlackIceInstance = require("../models/blackIceInstance.model.js");

exports.create = (req, res) => {
  if (!(req.body.id_blackIceData && req.body.id_floor)){
    res.status(400).send({message: "Malformed input for BlackIceInstance creation"});
    return;
  } else {
    BlackIceInstance.create(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        console.log(err);
        res.status(500).send({message: "Error creating BlackIceInstance"});
        return;
      });
  }
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  BlackIceInstance.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Cannot find BlackIceInstance with id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving BlackIceInstance with id=" + id});
      return;
    });
};

exports.findAll = (req, res) => {
  BlackIceInstance.findAll()
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `No BlackIceInstance found`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving all BlackIceInstances "});
      return;
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  BlackIceInstance.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "BlackIceInstance was updated successfully."});
      } else {
        res.send({message: `Cannot update BlackIceInstance with id=${id}. Maybe BlackIceInstance was not found or req.body is empty!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error updating BlackIceInstance with id=" + id});
      return;
    });
};

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  BlackIceInstance.destroy({
    where: { id: id },
    paranoid: false
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "BlackIceInstance was deleted successfully!"});
      } else {
        res.send({message: `Cannot delete BlackIceInstance with id=${id}. Maybe BlackIceInstance was not found!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Could not delete BlackIceInstance with id=" + id});
      return;
    });
};
