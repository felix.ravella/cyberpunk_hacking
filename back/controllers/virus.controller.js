const Virus = require("../models/virus.model.js");

exports.create = (req, res) => {
  if (req.body.challenge == null || req.body.label == null || req.body.id_network == null || req.body.id_netrunner == null){
    res.status(400).send({message: "Malformed input for Virus creation"});
    return;
  } else {
    Virus.create(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        console.log(err);
        res.status(500).send({message: "Error creating Virus"});
        return;
      });
  }
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  Virus.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Cannot find Virus with id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving Virus with id=" + id});
      return;
    });
};

exports.findAll = (req, res) => {
  Virus.findAll()
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `No Virus found`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving all Viruss "});
      return;
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Virus.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "Virus was updated successfully."});
      } else {
        res.send({message: `Cannot update Virus with id=${id}. Maybe Virus was not found or req.body is empty!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error updating Virus with id=" + id});
      return;
    });
};

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  Virus.destroy({
    where: { id: id },
    paranoid: false
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "Virus was deleted successfully!"});
      } else {
        res.send({message: `Cannot delete Virus with id=${id}. Maybe Virus was not found!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Could not delete Virus with id=" + id});
      return;
    });
};
