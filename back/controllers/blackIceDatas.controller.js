const BlackIceData = require("../models/blackIceData.model.js");

exports.create = (req, res) => {
  // Vérifie que le corps de la requete n'est pas vide
  if (!req.body) {
    res.status(400).send({message: "Le contenu ne peut pas être vide."});
    return;
  }
  // construit un objet de la classe Fiche 
  const blackIceData = {
    className: req.body.className,
    perception: req.body.perception,
    speed: req.body.speed,
    attack: req.body.attack,
    defense: req.body.defense,
    maxRez: req.body.maxRez,
    targetType: req.body.targetType,
    effectText: req.body.effectText
  }
  // lance le stockage en BDD
  BlackIceData.create(blackIceData)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({message: err.message || "Une erreur interne est survenue."});
      return;
    });
}

exports.findOne = (req, res) => {
  const id = req.params.id;
  BlackIceData.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Impossible de trouver la blackIceData avec id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: `Erreur serveur en cherchant la blackIceData d'id=${id}`});
      return;
    });
};

exports.findAll = (req, res) => {
  BlackIceData.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({message:err.message || `Erreur serveur en cherchant les blackIceData`});
      return;
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  BlackIceData.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "BlackIceData was updated successfully."});
      } else {
        res.send({message: `Cannot update BlackIceData with id=${id}. Maybe BlackIceData was not found or req.body is empty!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error updating BlackIceData with id=" + id});
      return;
    });
}

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  BlackIceData.destroy({
    where: { id: id },
    paranoid: false
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "BlackIceData was deleted successfully!"});
      } else {
        res.send({message: `Cannot delete BlackIceData with id=${id}. Maybe BlackIceData was not found!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Could not delete BlackIceData with id=" + id});
      return;
    });
};
