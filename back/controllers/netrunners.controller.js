const Netrunner = require("../models/netrunner.model.js");

exports.create = (req, res) => {
  if (req.body.name == null || req.body.skillRank == null){
    res.status(400).send({message: "Malformed input for Netrunner creation"});
    return;
  } else {
    Netrunner.create(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        console.log(err);
        res.status(500).send({message: "Error creating Netrunner"});
        return;
      });
  }
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  Netrunner.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Cannot find Netrunner with id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving Netrunner with id=" + id});
      return;
    });
};

exports.findAll = (req, res) => {
  Netrunner.findAll()
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `No Netrunner found`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving all Netrunners "});
      return;
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Netrunner.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "Netrunner was updated successfully."});
      } else {
        res.send({message: `Cannot update Netrunner with id=${id}. Maybe Netrunner was not found or req.body is empty!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error updating Netrunner with id=" + id});
      return;
    });
};

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  Netrunner.destroy({
    where: { id: id },
    paranoid: false
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "Netrunner was deleted successfully!"});
      } else {
        res.send({message: `Cannot delete Netrunner with id=${id}. Maybe Netrunner was not found!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Could not delete Netrunner with id=" + id});
      return;
    });
};
