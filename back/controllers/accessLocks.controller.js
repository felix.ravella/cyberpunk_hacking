const AccessLock = require("../models/accessLock.model.js");

exports.create = (req, res) => {
  if (req.body.challenge == null || req.body.id_floor == null){
    res.status(400).send({message: "Malformed input for AccessLock creation"});
    return;
  } else {
    AccessLock.create(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({message: "Error creating AccessLock"});
        return;
      });
  }
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  AccessLock.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Cannot find AccessLock with id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving AccessLock with id=" + id});
      return;
    });
};

exports.findAll = (req, res) => {
  AccessLock.findAll()
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `No AccessLock found`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving all AccessLocks "});
      return;
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  AccessLock.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "AccessLock was updated successfully."});
      } else {
        res.send({message: `Cannot update AccessLock with id=${id}. Maybe AccessLock was not found or req.body is empty!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error updating AccessLock with id=" + id});
      return;
    });
};

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  AccessLock.destroy({
    where: { id: id },
    paranoid: false
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "AccessLock was deleted successfully!"});
      } else {
        res.send({message: `Cannot delete AccessLock with id=${id}. Maybe AccessLock was not found!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Could not delete AccessLock with id=" + id});
      return;
    });
};
