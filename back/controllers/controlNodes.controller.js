const ControlNode = require("../models/controlNode.model.js");

exports.create = (req, res) => {
  if (req.body.targetLabel == null || req.body.challenge == null || req.body.id_floor == null){
    res.status(400).send({message: "Malformed input for ControlNode creation"});
    return;
  } else {
    ControlNode.create(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({message: "Error creating ControlNode"});
        return;
      });
  }
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  ControlNode.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Cannot find ControlNode with id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving ControlNode with id=" + id});
      return;
    });
};

exports.findAll = (req, res) => {
  ControlNode.findAll()
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `No ControlNode found`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving all ControlNodes "});
      return;
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  ControlNode.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "ControlNode was updated successfully."});
      } else {
        res.send({message: `Cannot update ControlNode with id=${id}. Maybe ControlNode was not found or req.body is empty!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error updating ControlNode with id=" + id});
      return;
    });
};

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  ControlNode.destroy({
    where: { id: id },
    paranoid: false
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "ControlNode was deleted successfully!"});
      } else {
        res.send({message: `Cannot delete ControlNode with id=${id}. Maybe ControlNode was not found!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Could not delete ControlNode with id=" + id});
      return;
    });
};
