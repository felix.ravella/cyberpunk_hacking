const DataFile = require("../models/dataFile.model.js");

const generateGibberish = require('../functions/gibberish_generator.js');

exports.create = (req, res) => {
  if (req.body.label == null || req.body.content == null || req.body.challenge == null || req.body.id_floor == null){
    res.status(400).send({message: "Malformed input for DataFile creation"});
    return;
  } else {
    let data = { cyphered_content: generateGibberish(), ...req.body };
    DataFile.create(data)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        console.log(err);
        res.status(500).send({message: "Error creating DataFile"});
        return;
      });
  }
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  DataFile.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Cannot find DataFile with id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving DataFile with id=" + id});
      return;
    });
};

exports.findAll = (req, res) => {
  DataFile.findAll()
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `No DataFile found`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving all DataFiles "});
      return;
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  DataFile.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "DataFile was updated successfully."});
      } else {
        res.send({message: `Cannot update DataFile with id=${id}. Maybe DataFile was not found or req.body is empty!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error updating DataFile with id=" + id});
      return;
    });
};

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  DataFile.destroy({
    where: { id: id },
    paranoid: false
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "DataFile was deleted successfully!"});
      } else {
        res.send({message: `Cannot delete DataFile with id=${id}. Maybe DataFile was not found!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Could not delete DataFile with id=" + id});
      return;
    });
};
