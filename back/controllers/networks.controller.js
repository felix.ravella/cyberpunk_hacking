const Network = require("../models/network.model.js");
const Floor = require("../models/floor.model.js");

const AccessLock = require("../models/accessLock.model.js");
const BlackIceInstance = require("../models/blackIceInstance.model.js");
const ControlNode = require("../models/controlNode.model.js");
const DataFile = require("../models/dataFile.model.js");

exports.create = (req, res) => {
  if (!(req.body.name && req.body.label)){
    res.status(400).send({message: "Malformed input for Network creation"});
    return;
  } else {
    Network.create(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({message: "Error creating Network"});
        return;
      });
  }
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  Network.findByPk(id, {
    include: [
      {
        model: Floor, include: [
          AccessLock,
          BlackIceInstance,
          ControlNode,
          DataFile
        ]}
    ]
  })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Cannot find Network with id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving Network with id=" + id});
      return;
    });
};

exports.findAll = (req, res) => {
  Network.findAll()
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `No Network found`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving all Networks "});
      return;
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Network.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "Network was updated successfully."});
      } else {
        res.send({message: `Cannot update Network with id=${id}. Maybe Network was not found or req.body is empty!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error updating Network with id=" + id});
      return;
    });
};

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  Network.destroy({
    where: { id: id },
    paranoid: false
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "Network was deleted successfully!"});
      } else {
        res.send({message: `Cannot delete Network with id=${id}. Maybe Network was not found!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Could not delete Network with id=" + id});
      return;
    });
};
