const Floor = require("../models/floor.model.js");

const AccessLock = require("../models/accessLock.model.js");
const BlackIceInstance = require("../models/blackIceInstance.model.js");
const ControlNode = require("../models/controlNode.model.js");
const DataFile = require("../models/dataFile.model.js");

exports.create = (req, res) => {
  if (!(req.body)){
    res.status(400).send({message: "Le corps de la requete est vide"});
    return;
  }
  if (req.body.depth == null && req.body.isBottom == null && req.body.id_network == null) {
    res.status(400).send({message: "Malformed input for Floor creation"});
    return;
  } else {
    Floor.create(req.body)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        console.log(err);
        res.status(500).send({message: "Error creating Floor"});
        return;
      });
  }
};

exports.findOne = (req, res) => {
  const id = req.params.id;
  Floor.findByPk(id, {
    include: [
      AccessLock,
      BlackIceInstance,
      ControlNode,
      DataFile
    ]
  })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `Cannot find Floor with id=${id}.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving Floor with id=" + id});
      return;
    });
};

exports.findAll = (req, res) => {
  Floor.findAll()
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `No Floor found`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error retrieving all Floors "});
      return;
    });
};

exports.findAllByNetworkId = (req, res) =>{
  let id = req.params.id;
  Floor.findAll({
    where: { id },
    include: [
      { model: AccessLock },
      { model: BlackIceInstance },
      { model: ControlNode },
      { model: DataFile },
    ]
  })
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({message: `No Floor found from network with id=${id}`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: `Error retrieving Floors from network with id=${id}` });
      return;
    });
}

exports.update = (req, res) => {
  const id = req.params.id;

  Floor.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "Floor was updated successfully."});
      } else {
        res.send({message: `Cannot update Floor with id=${id}. Maybe Floor was not found, or there was no modofication to apply.`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Error updating Floor with id=" + id});
      return;
    });
};

exports.deleteOne = (req, res) => {
  const id = req.params.id;
  Floor.destroy({
    where: { id: id },
    paranoid: false
  })
    .then(num => {
      if (num == 1) {
        res.send({message: "Floor was deleted successfully!"});
      } else {
        res.send({message: `Cannot delete Floor with id=${id}. Maybe Floor was not found!`});
        return;
      }
    })
    .catch(err => {
      res.status(500).send({message: "Could not delete Floor with id=" + id});
      return;
    });
};
