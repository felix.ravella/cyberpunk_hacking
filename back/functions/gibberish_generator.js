const dictionnary = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN&~#^@£$§!€*+-_";

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function generateGibberish() {
  const gibberishSize = 150 + getRandomInt(250);

  let gibberish = ""; 
  for (let i = 0; i < gibberishSize; i++){
    gibberish += dictionnary[getRandomInt(dictionnary.length)];
  }
  return gibberish;
}

module.exports = generateGibberish;

